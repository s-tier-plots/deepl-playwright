from playwright.sync_api import sync_playwright
import time
import os
import re
import pyperclip

def remove_timeline(txtpath):
	with open(txtpath, 'r', encoding='utf-8') as f:
		contents = f.read()
		# remove num + timeline
		sub1 = re.sub(r'\d+\n\d\d:\d\d:\d\d,\d\d\d --> \d\d:\d\d:\d\d,\d\d\d', '', contents, 0, re.M)
		sub1 = re.sub(r'\n', '', sub1, 1, re.M)
		sub1 = re.sub(r'\n\n', '\n', sub1, 0, re.M)

	os.path.splitext(txtpath)[0]
	with open(os.path.splitext(txtpath)[0] + '.og.txt', 'w', encoding='utf-8') as f:
		f.write(sub1)

	return os.path.splitext(txtpath)[0] + '.og.txt'


def combine_srt(mysrt, wordtxt, finalsrt):
	with open(mysrt, 'r', encoding='utf-8') as f:
		srt = f.read()
		match = re.findall(r'\d+:\d+:\d+,\d+ --> \d+:\d+:\d+,\d+', srt)

	linerList = []
	liner = ""
	with open(wordtxt, "r", encoding="utf-8", errors='ignore') as wordfile:
		lines = wordfile.readlines()
		for line in lines:
			if line != '\n' and line is not lines[-1]:
				liner += line
			elif line != '\n' and len(linerList) == len(match)-1:
				liner += line
				linerList.append(liner)
				break
			else:
				linerList.append(liner)
				liner = ""

	count = 0
	with open(finalsrt, 'w', encoding='utf-8') as resfile:
		for timeline in match:
			resfile.write(f"{count+1}\n")
			resfile.write(timeline+'\n')
			resfile.write(linerList[count])
			resfile.write("\n")
			count += 1

def paste_it(page, chunk):
	input_area = page.get_by_role("textbox", name="Source text").get_by_role("paragraph").first
	pyperclip.copy(chunk)
	input_area.fill('')
	page.keyboard.press('Control+v')
	numC = len(chunk)
	if numC < 500:
		time.sleep(6)
	elif numC < 1546:
		time.sleep(10)
	elif numC < 2635:
		time.sleep(10)
	elif numC < 4000:
		time.sleep(17)
	else:
		time.sleep(19)

	button = page.get_by_role("button", name="Copy to clipboard")
	time.sleep(2)
	button.click()
	time.sleep(1)
	content = pyperclip.paste()
	input_area.fill('')
	return content

deepl_url = 'https://www.deepl.com/translator#'
deepl_toEn = '/EN/%0A'
print('Enter source lang:')
source_lang = input()
max_len = 3000

# init
with sync_playwright() as p:
	srtpath = os.getcwd()
	listsrt = [os.path.join(srtpath, f) for f in os.listdir(srtpath) if f.endswith('.srt')]

	browser = p.chromium.launch(headless=False)
	context = browser.new_context()
	page = browser.new_page()
	page.goto(deepl_url + source_lang + deepl_toEn)
	page.get_by_text("Close").click()
	
	listsrt = [os.path.join(srtpath, f) for f in os.listdir(srtpath) if f.endswith('.srt')]

	for srt in listsrt:
		txtpath = remove_timeline(srt)
		with open(txtpath, 'r', encoding='utf-8') as f:
			all_text = f.read()	

		sentence = ""
		list_sentence = []
		chunk = ""
		chunks = []
		finaltext = ""
		content = ""
		counter = 1

		print(f'Working on {os.path.basename(srt)}')
		if len(all_text) < max_len:
			print('the whole text can be pasted')
			content = paste_it(page, all_text)
			#content = re.sub(r'\nTranslated with www\.DeepL\.com/Translator \(free version\)', '', content, 0, re.M)
			finaltext += content
			pyperclip.copy('')
		else:
			# list of lines
			with open(txtpath, 'r', encoding='utf-8') as f:
				lines = f.readlines()

			
			for line in lines:
				if line != '\n' and line is not lines[-1]:
					sentence += line
				else:
					sentence += line
					list_sentence.append(sentence)
					sentence = ""

			for sentence in list_sentence:
				if len(chunk) <= max_len - 50 and sentence is not list_sentence[-1]:
					chunk += sentence
				elif sentence is list_sentence[-1]:
					chunk += sentence
					chunks.append(chunk)
					chunk = ""
				else:
					chunks.append(chunk)
					chunk = ""
					chunk += sentence


			for chunk in chunks:
				chunk = ''.join(chunk)
				content = paste_it(page, chunk)
				finaltext += content
				'''
				with open(srtpath+f'\\chnk_{os.path.basename(srt)}-0{counter}.txt', 'w', encoding='utf-8-sig') as g:
						g.write(content)

				with open(srtpath+f'\\chnk_{os.path.basename(srt)}-0{counter}.txt', 'r', encoding='utf-8-sig') as g:
						lines = g.read()
						xx = lines.replace("\n\n", "\n")

				with open(srtpath+f'\\chnk_{os.path.basename(srt)}-0{counter}.txt', 'w', encoding='utf-8-sig') as g:
						g.write(xx)

				counter += 1
				print(f'CHUNK is')
				print(f'{chunk}')
				print(f'content is')
				print(f'{content}')
				'''
				print(f'chunk {counter} is done')
				counter += 1
				content = ""
				pyperclip.copy('')

		completeName = os.path.splitext(srt)[0] + '.en.txt'
		finaltext = finaltext.replace("\nTranslated with www.DeepL.com/Translator (free version)", "")
		
		with open(completeName, 'w', encoding='utf-8') as g:
				g.write(finaltext)

		with open(completeName, 'r', encoding='utf-8') as g:
				finaltext2 = g.read()
				finaltext3 = finaltext2.replace("\n\n", "\n")
				finaltext3 = finaltext3.replace("\n\n\n", "\n")

		with open(completeName, 'w', encoding='utf-8') as g:
				g.write(finaltext3)

		finalsrt = os.path.splitext(srt)[0] + '.dl.en.srt'
		try:
			combine_srt(srt, completeName, finalsrt)
		except IndexError:
			print("ERROR FAILED")
			continue
		os.remove(completeName)
		os.remove(txtpath)

		print(f'{os.path.basename(finalsrt)} IS COMPLETED')
	browser.close();





